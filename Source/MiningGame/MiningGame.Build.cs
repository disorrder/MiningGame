// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class MiningGame : ModuleRules
{
	public MiningGame(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore" });
	}
}
