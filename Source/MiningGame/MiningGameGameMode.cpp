// Copyright Epic Games, Inc. All Rights Reserved.

#include "MiningGameGameMode.h"
#include "MiningGameCharacter.h"
#include "UObject/ConstructorHelpers.h"

AMiningGameGameMode::AMiningGameGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/SideScrollerCPP/Blueprints/SideScrollerCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
