// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MiningGameGameMode.generated.h"

UCLASS(minimalapi)
class AMiningGameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMiningGameGameMode();
};



